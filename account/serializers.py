from rest_framework import serializers
from account.models import User
from django.contrib.auth.hashers import make_password
class UserRSerializer(serializers.HyperlinkedModelSerializer):
    password2 = serializers.CharField(style={'input_type':'password'},write_only=True)
    class Meta:
        model = User
        fields = ['email','name','password','password2','tc']
        extra_kwargs = {'password':{'write_only':True}}
        def create(self, validated_data):
            #if validated_data.get('password') != validated_data.get('password2'):
             #   raise serializers.ValidationError("Those password don't match") 
            #elif validated_data.get('password') == validated_data.get('password2'):
             #   validated_data['password'] = make_password(validated_data.get('password'))
            return super(UserRSerializer, self).create(validated_data)

class UserLSerializer(serializers.HyperlinkedModelSerializer):
    email = serializers.EmailField(max_length=255)
    class Meta:
        model = User
        fields = ['email','password']


